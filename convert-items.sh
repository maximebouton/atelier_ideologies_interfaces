#!/bin/sh

for dir in items/*
do
    echo 
    echo $dir
    if [ -f "$dir/README.md" ]; then
        echo "converting README.md"
        pandoc -s $dir/README.md -o $dir/README.html
    else
        echo "No README.md found"
    fi
done
