https://etherpad.net/p/presentation_ESADSE

Bonjour,

- Nous sommes Maxime et Émile, on vient cette semaine pour proposer un atelier

un travail de recherche
  sur la  
  la question des idéologies des interfaces.

- Déja RandomLab Jérémie Nuel
- Regard critique sur les interfaces, la culture informatique



intro
- indépendants
- étudiants
- 2


__critiques__

- coincoin
  * blockchain
  * marre
- terminator
  * dieter rams + google
  * lialina
- 0lib
  * archive
  * bot

__outils__

- dactyplodocus
  * vidéo
  * images
- plenty of room
  * vidéo
  * images
- tpxt
  * vidéo
  * images



__bonus__

- TRUE MOUSE TRACKING


- pratique perso

---
    # Nous


Bonjour, 

On est Émile et Maxime

on propose cette semaine un atelier : un travail de recherche sur les interfaces et leurs idéologies

nous sommes sortis de l'école depuis un an et quelques mois maintenant

nous avons fait nos études à l'ÉSAD Valence en option design graphique

pendant notre dernière année, nous avons commencé à pas mal travailler ensemble

On a fait notre mémoire et notre diplôme ensemble

depuis on travaille ensemble ou avec d'autres personnes
sur des petites commandes, des projets de recherche en design, des ateliers, etc.

On porte ensemble un regard critique sur l'informatique actuelle (son design) ?

Sur une attitude critique vis à vis des pratiques dominantes / dominatrices du design d'interface, du web design, de l'informatique, du discours qui les accompagne. 



    # Pratique


- On cherche à contre-dire l'unification des usages
- tendances à opacifié le fonctionnement et à la fermeture des objets techniques
- l'influence des interfaces sur nos usages et notre compréhension de l'ordinateur (du monde)
- la promotion/l'injonction à la simplicité, à l'efficacité et à la sécurité sans doute au détriment de l'ouverture et des possibilités
- En générale les pratiques de design qui tendent à définir l'usage (à le conditioner). Pratiques qui décident de la finalité d'un objet à la place de son destinataire.


__0lib__

*Archive d'images non-commentées et non likées d'Instagram*

- Accès algorithmique à l'information sur le web et les réseaux sociaux (tendances, recommandés, proximités, ressemblances, etc.) Youtube, Google, etc.
- Like et non dislike = croissance innévitable
- Quelle est la valeur ? Quelle est la rareté ?
- Confort, confronter / ignorer
- les bots, les communautés, outils vente
- une archive live ? 


__Plenty of room__

- un outil de dessin et d'écriture collectif
- la recherce d'un outil ou l'utilisateur définisse lui-même l'utilité de l'objet voir intervienne le plus possible dans sa forme.
- un objets libre à l'interpretation, suggérant une réapropriation plutôt qu'un usage définit.
- un objets générique sans but particulier.
- usage illimité du zoom rend contextuelle la hierarchisation du contenu, permet un assemblage de contenu sans modération (toujours de la place, difficile de faire disparaitre un contenu)
- ambigueté spatiale entre surface et profondeur, rapport inhabituel à la page

modèle d'organisation d'information

__CoinCoin__

- blockchain minimale, locale, temporaire
- cryptomonnaie mise en place pendant un atelier d'une semaine

- on a chercher une manière de décortiquer la promesse d'une confiance logicielle inffaillible que représente la blockchain

On cherche pas à faire du "bon design" dans le sens d'un design qui serait voué à la résolution de problèmes.

- fonction d'une transaction ?
- Cryptomonnaire destinée à produire de la musique 
- un travail qui met en dérision les "règles" qui permettent à une blockchain de fonctionner
- forme influencée par les transactions, visualisation absurde des échanges (ya à peu près N coin coin)
- Musique définie par les données monétaires de la blockchain, sa configuration : quantitée total de valeurs, nombre d'échanges ect...
- valeur de la monnaie, une valeur de production d'un objet


__tpxt__

Dans ce contexte on s'interesse aux pratiques amatrices du design, à des objets à première vue désuets ou obsolets.


__Dactyloplodocus__

On a interet pour la mise en valeur des formes techniques du design des programmes, leur écriture (live-coding).

*Programme de live-coding génération forme graphique, matrice, trame basé sur l'algorithme floodfill, etc.*

- Interfaces dits graphiques (GUI) devenus la norme (métaphore du bureau)
- Autres paradigmes (TUI, CLI, etc.).
- Modèles de penser le rapport à l'ordinateur.
- Est-ce que la non-présence du terme "graphique" *sugère* que TUI, CLI pas graphique ?
- Potentiel graphique du texte ?
- Floodfill = remplissage, pot de peinture (paint, photoshop, etc.), exécuter de manière très (trop) rapide
- Place de l'erreur (multi-joueur, ram, log, etc.)
- Dépassement de l'aspect strictement fonctionnel de l'algorithme floodfill pour la production de forme graphiques (détournement)


__Terminator__

On à tendance à voir l'objet technique comme une forme de discours, une forme rhétorique et pas comme une forme objective, purement fonctionnelle.

- Olia lialina décrit la disparition du terme "utilisateur" et de la notion d'"ordinateur". 
- Elle insiste sur l'importance des termes et méthodes employés par les designers dans ce phénomène.

- l'utilisateur gène car il détourne le design, il reprends le contrôle, sans ordinateur (smart-phone plutot qu'ordinateur-mobile) pas do'bjet à contrôler, determination de l'objet technique.

Sans une culture de l'utilisateur, pas de web amateur,  gentrification du web avec l'usage majoritaire de quelques sites à l'usage très conditionné ( google, facebook,  youtube.)

user          -> people
computer -> technology
interface  -> experience

- tend vers l'abstraction (figure de style : synecdoque généralisante)
- l'ordinateur en tant qu'outil programmé et reprogrammable

- Suite à cette réflexion pour notre diplome nous avons présenter un bookmarlet (un script) de navigateur qui renverse ce remplacement dans le contenu des pages web consultées.

- google ???


Conclulol
- outils d'écriture et  objets critiques
- pratique personelle / commandes
- recherche hors d'un cadre institutionel

    # Atelier


on aimerait avec cet atelier proposer une tentative de recherche sur les idéologies des interfaces, chercher à décortiquer les systèmes de pensées à l'oeuvre dans le design des interfaces, chercher à comprendre quelle histoire économique, politique, sociale, culturelle voir religieuse mène à ces attitudes de design dominatrices.




