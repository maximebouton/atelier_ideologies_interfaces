
présentation rapide de nous (20min)
-----------------------------------

- cv ? Qui sommes-nous
- projets, qu'avons-nous fait
- courte présentation de l'atelier, qu'allons-nous proposer pendant la semaine


présentation du sujet (démarrage de l'atelier)
----------------------------------------------

- la thématique
  + c'est quoi les ideologie de l'interface / objets techniques

- notre recherche (objets, amorces de publications/textes) sur la thématique
  + qu'est ce que nous on en a fait

- la thématique dans le contexte de l'atelier, du randomLab
  + qu'est ce qu'on en fait ici et pourquoi ?
    * tentative de définition d'un programme, d'un collectif de recherche à
      plus long terme 

- présentation des outils, livres, etherpad, ressources, comment acceder à un
  raspberry, generation du document


materiel, objets présents dans l'atelier
----------------------------------------

- une table avec des livres organisés à l'avance
- raspberry avec dossier ressources
- écran + clavier + projection pour acceder au raspberry


les objets
----------

ce à quoi sont invités les participants, les groupes.
Des objets argumentatifs (support à l'argumentation, à la construction d'une
pensée, sur la thématique).

- textes
  + un court article
  + d'une traduction
  + interview, entretient
  + d'un compte-rendu de lecture
  + texte séléctionné, extrait

- éléments méthodologiques
  + notes de lecture
  + une bibliographie
  + une iconographie
  + documentation sur un objet
  + un ensemble de sources, références ou citations

- traces libres
  + un objet de design ou un objet libre, documenté
  + forme textuelle libre


documentation restitution
-------------------------

assemblage, confrontation des travaux, sous forme de matière/sources
réutilisables par la suite.


a faire avant l'atelier
-----------------------

- constituer une bibliographie, liste de livres à emmener
- logiciel de regroupement des contenus et de mise en page 
- dossier ressources


Introduction de l'atelier
-------------------------

tour de table (nous inclus, l'un de nous commence) : 

- prénom formation
- pourquoi vous avez choisi cet atelier ?
- quels sont vos attentes ?

Proposer de lire le texte d'introduction, une phrase, une partie ?

Prises de paroles spontanées ?

tour de table : 

- vous en pensez-quoi ?
- pistes, avis ?

entamer une discution sur le sujet

à la fin de la journée ou journée suivante (ou si manque théorique à un
moment), es-ce-que vous avez besoin d'un apport théorique ? Des préçisions sur
notre démarche ?

préparer des vidéos, sites à montrer, extraits de discours

ou à donner à voir durant une journée

droit au but efficace
