// FUNCTIONS

function replaceSrc(item, html) {
    var imgs = html.getElementsByTagName("img");
    for (var i = 0; i < imgs.length; i++) {
        var src = imgs[i].getAttribute("src");
        imgs[i].src = "../items/" + item + "/images/" + src;
    }
    return html;
}

function createIntro(url) {
    fetch(url)
        .then(function (response) {
            return response.text();
        })
        .then(function(text) {
            document.getElementById("intro").innerHTML +=text;
        })
        .catch(function(err) {  
            console.log(err);  
        });
}

function createIndex(items) {
    
    for (let i = 0; i < items.length; i++) {

        console.log(items[i]);
        
        fetch("../items/" + items[i] + "/metadata.json")
            .then(function (response) {
                return response.json();
            })
            .then(function(json) {
                console.log(items[i]);
                var div = document.createElement("div");
                var a = document.createElement("a");
                a.href = "../items/" + items[i] + "/README.html";
                a.innerText += json.titre;
                a.innerText += ' — ';
                for (var j = 0; j < json.auteurs.length; j ++) {
                    a.innerText += json.auteurs[j];
                    if (j == json.auteurs.length-1) {
                        a.innerText += '.';
                    } else {
                        a.innerText += ', ';
                    }
                }
                div.appendChild(a);
                document.getElementById("index").appendChild(div);
            })
            .catch(function(err) {  
                console.log(err);  
            });
    }
}

function createItems(items) {

    for (let i = 0; i < items.length; i++) {

        console.log(items[i]);

        fetch("../items/" + items[i] + "/README.html")
            .then(function (response) {
                return response.text();
            })
            .then(function(text) {
                var parser = new DOMParser();
                var html = parser.parseFromString(text, "text/html");
                var html = replaceSrc(items[i], html);
                var serializer = new XMLSerializer();
                var text = serializer.serializeToString(html.body);
                var div = document.createElement("div");
                div.id = "item-" + items[i];
                div.innerHTML += text;
                document.getElementById("items").appendChild(div);

                // get styles
            })
            .catch(function(err) {  
                console.log(err);  
            });
    }
}

// VARIABLES

var items = [
    "spectres-du-numerique",
    "des-interfaces-a-notre-image",
    "interface-et-sience-fiction",
    "jeannot-lapin",
    "helo",
    "rpgmaker",
    "google-recherche",
    "exploitation-des-donnees-personelles",
    "theatre-et-interfaces",
    "frontieres-numeriques-et-migrations",
    "projet-stalk"

];

// INIT

/*createIntro('../INTRO.html');*/
createIndex(items);
createItems(items);
