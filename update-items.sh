#!/bin/sh

for dir in items/*
do
    echo 
    echo $dir
    if [ -f "$dir/metadata.json" ]; then
        echo "downloading pad"
        pad=$(cat "$dir/metadata.json" | jq '.pad')
        pad=${pad:1:-1}
        echo $pad
        echo $dir/README.md
        curl "$pad/export/txt" -o $dir/README.md
    else
        echo "No metadata found"
    fi
done


