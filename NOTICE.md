Atelier d'initiation à la recherche : "Idéologies et utopies des interfaces".

Les interfaces homme-machine comme tout objet technique ou de design sont aussi
des formes de discours. Des formes rhétoriques qui agissent autant dans l'usage
que dans le résultat. De l'organisation des espaces personnels et intimes sous
la forme de bureaux ou de flux d'actualité à l'impact esthétique dans les
productions de design de l'usage des logiciels de création assisté par
ordinateur ; les interfaces transmettent un ensemble d'idées, de rapports
utopiques à l'organisation sociale, au vivant, à sa fonction et sa
perfectibilité. Derrière le mythe de leur objectivité existe une longue histoire
de luttes et d'intérêts individuels, d'idéaux parfois très personnels. Bien
qu'il soit évident que les systèmes de pensée aux origines de ces objets n'ont
rien à voir avec les positions culturelles ou politiques des usagers, on peut
se demander si au fil du temps, la présence majoritaire d'un système plutôt que
d'un autre ne cause pas de profondes modifications.

Dans le contexte de l'élaboration d'un travail de recherche qui se poursuivra avec le
Random-Lab de L'ESADSE intitulé "Idéologies des interfaces", l'objectif de cet
atelier est de développer avec les étudiants un rapport critique aux objets de
design par l'étude des interfaces homme-machine.

Les étudiant·es seront invité·es à fournir un travail théorique argumenté
durant la semaine.

L'atelier débutera par une présentation plus approfondie de la thématique et par la définition des pistes de travail.
Des temps de mise en communs seront prévus afin d'échanger  sur l'ensemble des travaux. Ces derniers seront restitués sous la forme d'une revue imprimée (environ 50 exemplaires, petit format) et d'un dossier en ligne.

Sans contrainte particulière en terme de qualité ou de quantité, les
étudiant·es pourront travailler seul·es ou en groupe sur l'élaboration, (au
choix) : 

- d'un texte
- d'un compte-rendu de lecture
- d'une traduction
- d'une bibliographie
- d'une iconographie
- d'un ensemble de sources, références ou citations
- d'un objet de design documenté

à partir de pistes historiques, critiques, politiques, sociologiques ... axées
sur les utopies et idéologies transmises par les interfaces homme-machines.

Quelques pistes thématiques :

Mémoire, ordinateur, tablette, téléphone, clavier, bureau, application,
internet, navigateur, moteur de recherche, fils d'actualité, système de
crédits sociaux, informatique et web amateur, pirates, cyberpunk,
cyberféminisme, cybernétique, langage naturel, intelligence artificielle,
informatique ubiquitaire, réalité augmentée, réalité virtuelle, blockchain,
smart-phone, smart-university, design centré humain, design d'expérience
utilisateur, design cognitif, flat-design, skeuomorphisme, Material Design,
IBM, Xerox, Microsoft, Apple, Google, Facebook, Twitter, Instagram.

Quelques références : 

Textes :

- "Comment reconnaitre la religion d'un ordinateur ?", dans "Comment voyager avec un saumon", Umberto Eco, titre original : "Il secondo diario minimo", Éditions Bompiani, Milan, 1992, Éditions Grasset & Fasquelle, 1997, pour la traduction française.
- "Turing complete user", Olia Lialina, publié sur contemporary-home-computing.org 2012.
- "A Cyborg Manifesto", Donna Haraway, Socialist Review, 1985.
- "$(echo echo) echo $(echo) - Command-line poetics", Florian Cramer, publié sur reader.lgru.net, 2007.

Vidéos :

- "Computers for Cynics", Ted Nelson, série de vidéos publiée sur youtube.com, 2012.

Objets :

- "TempleOS", Terry A. Davis, 2005 (as J Operating System), 2013 (as TempleOS), dernière version 5.03 / 20 novembre 2017.
- "Atlas critique d'internet", Louise Drühle, Diplôme ENSAD 2015
- David Benque : the architecures of choice

en vrac :

- https://linc.cnil.fr/
- https://www.laquadrature.net/2019/10/17/le-conseil-detat-autorise-la-cnil-a-ignorer-le-rgpd/
- https://www.gobolinux.org/
- https://www.undernews.fr/reseau-securite/privacy-by-design-une-composante-cle-du-rgpd.html

the interface effect A.GALLOWAY :

- http://art.yale.edu/file_columns/0000/1404/galloway_alexander_r_-_the_interface_effect.pdf
- https://www.cairn.info/revue-communication-et-langages1-2013-1-page-155.htm#
- http://manovich.net/index.php/projects/on-totalitarian-interactivity
